package com.buutcamp.entity.manytomany;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="course_manytomany")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToMany(fetch=FetchType.LAZY, cascade = {CascadeType.DETACH,
                                                CascadeType.PERSIST,
                                                CascadeType.MERGE,
                                                CascadeType.REFRESH})
    @JoinTable(name="student_course",
                        joinColumns = @JoinColumn(name="course_id"),
                        inverseJoinColumns = @JoinColumn(name="student_id")) //references other table
    private List<Student> students;

    @Column(name="course_name")
    private String courseName;

    public Course() {
    }

    public Course(String courseName) {
        this.courseName = courseName;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", courseName='" + courseName + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}
