package com.buutcamp.entity.onetomany;

import com.buutcamp.entity.onetoone.StudentDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="instructor")
public class Instructor {

    //Define this as hibernate mapped object
    //map to table
    //create fields to manage
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="first_name")
    private String firstname;

    @Column(name="last_name")
    private String lastName;

    @Column(name="age")
    private int age;

    @OneToMany(cascade = CascadeType.ALL,
                fetch = FetchType.LAZY)
    @JoinColumn(name="course_id")
    private List<Course> courses;



    public Instructor(String firstname, String lastName, int age) {
        //this.id = id;
        this.firstname = firstname;
        this.lastName = lastName;
        this.age = age;
    }

    public Instructor() {
    }

    public void addCourse(Course course) {

        if (this.courses == null) {
            this.courses = new ArrayList<Course>();
        }
        this.courses.add(course);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Instructor{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", courses=" + courses +
                '}';
    }


    //annotate fields
    //generate getters, setters and constructors
}
