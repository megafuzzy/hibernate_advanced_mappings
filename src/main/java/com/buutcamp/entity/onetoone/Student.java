package com.buutcamp.entity.onetoone;

import javax.persistence.*;

@Entity
@Table(name="student")
public class Student {

    //Define this as hibernate mapped object
    //map to table
    //create fields to manage
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="first_name")
    private String firstname;

    @Column(name="last_name")
    private String lastName;

    @Column(name="age")
    private int age;

    @OneToOne(cascade = CascadeType.ALL) //this is ALL by default as well
    @JoinColumn(name="studentdetails_id") //db table column name
    private StudentDetails studentDetails;

    public Student(String firstname, String lastName, int age) {
        //this.id = id;
        this.firstname = firstname;
        this.lastName = lastName;
        this.age = age;
    }

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public StudentDetails getStudentDetails() {
        return studentDetails;
    }

    public void setStudentDetails(StudentDetails studentDetails) {
        this.studentDetails = studentDetails;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", studentDetails=" + studentDetails +
                '}';
    }

//annotate fields
    //generate getters, setters and constructors
}
