package com.buutcamp.main;

import com.buutcamp.entity.onetomany.Course;
import com.buutcamp.entity.onetomany.Instructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ManyToOneDemo {

    public static void main(String[] args) {
        //Configure hibernate
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        //get a new session
        Session session = sessionFactory.getCurrentSession();
        //create new student and its details
        session.beginTransaction();

        Instructor instructor = new Instructor("Severus", "Snape", 50);
        Course course1 = new Course("Healing potions");
        Course course2 = new Course("Dark arts defence");



        Instructor instructor2 = new Instructor("Larry", "Crimson", 43);
        Course course11 = new Course("Philosophy");
        Course course21 = new Course("History 3");
        //Course course3 = new Course("Healing potions");



        instructor.addCourse(course1);
        instructor.addCourse(course2);

        session.save(instructor);



        instructor2.addCourse(course11);
        instructor2.addCourse(course21);

        session.save(instructor2);

        course1.setInstructor(instructor);
        course2.setInstructor(instructor);

        course11.setInstructor(instructor2);
        course21.setInstructor(instructor2);




  /*      int theId = 3;
        Course course = session.get(Course.class, theId);
        System.out.println(course.toString());
        System.out.println("\nInstructor"+course.getInstructor().toString());*/

        /*


        Student student = new Student("Rowan", "Atkinson", 35);
        StudentDetails studentDetails = new StudentDetails("r.atkinson@gmail.com");
        student.setStudentDetails(studentDetails);
        session.save(student);
        */
        //save data to database
        //close the session
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();


    }
}

