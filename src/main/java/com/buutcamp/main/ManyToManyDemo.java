package com.buutcamp.main;

import com.buutcamp.entity.manytomany.Course;
import com.buutcamp.entity.manytomany.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ManyToManyDemo {

    public static void main(String[] args) {
        //Configure hibernate
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        //get a new session
        Session session = sessionFactory.getCurrentSession();
        //create new student and its details
        session.beginTransaction();
/*
        Course course1 = new Course("History");
        Course course2 = new Course("Biology");
        Course course3 = new Course("Geography");

        Student student1 = new Student("Eager", "Student");
        Student student2 = new Student("Lazy", "Bum");
        Student student3 = new Student("Sleeping", "One");

        student1.addCourse(course1);
        student1.addCourse(course3);

        student2.addCourse(course1);
        student2.addCourse(course2);

        student3.addCourse(course1);
        student3.addCourse(course3);
        student3.addCourse(course2);

        session.save(course1);
        session.save(course2);
        session.save(course3);

        session.save(student1);
        session.save(student2);
        session.save(student3);*/

        int theId=3;
        Student student = session.get(Student.class, theId);
        System.out.println("Student: "+student);
        System.out.println("Student courses: "+student.getCourses());

        Course course = session.get(Course.class, theId);
        System.out.println("Course: "+course);
        System.out.println("Course students: "+course.getStudents());

        session.getTransaction().commit();
        session.close();
        sessionFactory.close();

    }


}