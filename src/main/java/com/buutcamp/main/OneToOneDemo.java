package com.buutcamp.main;

import com.buutcamp.entity.onetoone.Student;
import com.buutcamp.entity.onetoone.StudentDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class OneToOneDemo {

    public static void main(String[] args) {
        //Configure hibernate
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(StudentDetails.class)
                .buildSessionFactory();

        //get a new session
        Session session = sessionFactory.getCurrentSession();
        //create new student and its details
        session.beginTransaction();

        int theId = 1;
        Student student = session.get(Student.class, theId);
        System.out.println(student.toString());


        /*
        Student student = new Student("Rowan", "Atkinson", 35);
        StudentDetails studentDetails = new StudentDetails("r.atkinson@gmail.com");
        student.setStudentDetails(studentDetails);
        session.save(student);
        */
        //save data to database
        //close the session
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }
}
