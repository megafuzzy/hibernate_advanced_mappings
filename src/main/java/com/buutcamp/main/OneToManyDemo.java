package com.buutcamp.main;

import com.buutcamp.entity.onetomany.Course;
import com.buutcamp.entity.onetomany.Instructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class OneToManyDemo {

    public static void main(String[] args) {
        //Configure hibernate
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        //get a new session
        Session session = sessionFactory.getCurrentSession();
        //create new student and its details
        session.beginTransaction();

       /* Instructor instructor = new Instructor("Severus", "Snape", 50);
        Course course1 = new Course("Healing potions");
        Course course2 = new Course("Dark arts defence");
        //Course course3 = new Course("Healing potions");

        instructor.addCourse(course1);
        instructor.addCourse(course2);

        session.save(instructor);*/


        int theId = 1;
        Instructor instructorGet = session.get(Instructor.class, theId);
        System.out.println(instructorGet.toString());
        System.out.println("Instructor object: "+instructorGet);


        /*
        Student student = new Student("Rowan", "Atkinson", 35);
        StudentDetails studentDetails = new StudentDetails("r.atkinson@gmail.com");
        student.setStudentDetails(studentDetails);
        session.save(student);
        */
        //save data to database
        //close the session
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }
}

